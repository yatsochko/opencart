<footer>
    <div class="container">
        <div class="row">
            <amp-accordion disable-session-states expand-single-section animate>
                <section>
                    <h5><?php echo $text_information; ?></h5>
                    <div><ul class="list-unstyled">
                        <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                    </ul></div>
                </section>
                <section>
                    <h5><?php echo $text_extra; ?></h5>
                    <div><ul class="list-unstyled">
                        <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                        <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                        <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
                        <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                    </ul></div>
                </section>
                <section>
                    <h5><?php echo $text_account; ?></h5>
                    <div><ul class="list-unstyled">
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                    </ul></div>
                </section>
            </amp-accordion>
        </div>
    </div>
</footer>
<form id="add_cart_form" method="post" action-xhr="index.php?route=checkout/cart/add" target="_top"
      on="submit-success:AMP.setState({total_cart_val: event.response.total,text_snackbar1:event.response.message}),snackbarSlideIn1.restart,cartList.refresh,lightbox0.close">
    <input type="hidden" name="product_id" value="0" [value]="add_cart_id" required>
    <input type="hidden" name="quantity" value="1" required>
</form>
<form id="currency_form" method="post" action-xhr="index.php?route=common/currency/currency" target="_top"
      on="submit-success:AMP.navigateTo(url='/')">
    <input type="hidden" name="code" value="0" [value]="currency_id" required>
</form>
<form id="language_form" method="post" action-xhr="index.php?route=common/language/language" target="_top"
      on="submit-success:AMP.navigateTo(url='/')">
    <input type="hidden" name="code" value="0" [value]="language_id" required>
</form>
<form id="wishlist_form" method="post" action-xhr="index.php?route=account/wishlist/add" target="_top"
      on="submit-success:AMP.setState({wishlist_val: event.response.total,text_snackbar1:event.response.message}),snackbarSlideIn1.restart,lightbox0.close">
    <input type="hidden" name="product_id" value="0" [value]="wishlist_id" required>
</form>


<amp-lightbox id="lightbox0" layout="nodisplay">
    <div class="lightbox"  >Загрузка...</div>
</amp-lightbox>

<amp-lightbox id="lightbox1" layout="nodisplay">
    <div class="lightbox"  >
        <div style="position:relative;max-width:1170px;width: 100%;height: 100%;margin:0 auto;padding-left:16px">
            <span on="tap:lightbox1.close" role="button" tabindex="0" style="position:absolute;right:0;color:white;top:0;height:48px;width:48px;text-align:center;line-height:48px;font-size:30px">✕</span>
            <div>
                <h2>Корзина</h2>
                <amp-list id="cartList" reset-on-refresh layout="fixed-height" height="480" src="index.php?route=api/json/cart" binding="no">
                    <template type="amp-mustache">
                        {{#item}}
                        <div class="product">
                            <amp-img width="80" height="80" alt="{{name}}" src="{{thumb}}"></amp-img>
                            <div>
                                <div>{{quantity}}x</div>
                                <div><strong>{{name}}</strong></div>
                                <div><i>${{price}}</i></div>
                            </div>
                        </div>
                        {{/item}}
                    </template>
                </amp-list>
            </div>
        </div>
    </div>
</amp-lightbox>



<amp-animation id="snackbarSlideIn1" layout="nodisplay">
    <script type="application/json">
        [
            {
                "duration": "5s",
                "fill": "both",
                "easing": "ease-out",
                "iterations": "1",
                "selector": "#snackbar1",
                "keyframes": [
                    {
                        "transform": "translateY(100%)"
                    },
                    {
                        "transform": "translateY(0)",
                        "offset": 0.05
                    },
                    {
                        "transform": "translateY(0)",
                        "offset": 0.95
                    },
                    {
                        "transform": "translateY(100%)"
                    }
                ]
            }
        ]
    </script>
</amp-animation>

<div class="snackbar" id="snackbar1">
    <span [text]="text_snackbar1" >snackbar2</span>
    <button on="tap:snackbarSlideIn1.finish" style="position:absolute;padding:24px 30px;font-size:1.8rem;right:0;top:0;background:none;color:white;border:none;cursor: pointer">✕</button>
</div>

<amp-animation id="snackbarSlideIn2" layout="nodisplay">
    <script type="application/json">
        [
            {
                "duration": "5s",
                "fill": "both",
                "easing": "ease-out",
                "iterations": "1",
                "selector": "#snackbar2",
                "keyframes": [
                    {
                        "transform": "translateY(-100%)"
                    },
                    {
                        "transform": "translateY(0)",
                        "offset": 0.05
                    },
                    {
                        "transform": "translateY(0)",
                        "offset": 0.95
                    },
                    {
                        "transform": "translateY(-100%)"
                    }
                ]
            }
        ]
    </script>
</amp-animation>
<div class="snackbar" id="snackbar2">
    <span [text]="text_snackbar2" >snackbar2</span>
    <button on="tap:snackbarSlideIn2.finish" style="position:absolute;padding:8px;font-size:1.6rem;right:0;top:0;background:none;color:white;border:none;cursor: pointer">✕</button>
</div>


<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>