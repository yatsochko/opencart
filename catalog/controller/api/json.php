<?php
class ControllerApiJson extends Controller {
	public function favorite() {
	    $json = '{
  "items": [{
    "id": 1,
    "img": "/image/cache/catalog/blackstr/img01035-200x200.jpg",
    "name": "Спортивные штаны",
    "price": "1.99",
    "stars": "XS / S / M / L / XL",
    "attribution": "visualhunt",
    "url": "#",
    "color": "green"
  }, {
    "id": 2,
    "img": "/image/cache/catalog/clothes/img01133-200x200.jpg",
    "name": "Джинсы",
    "attribution": "visualhunt",
    "price": "0.99",
    "stars": "29 / 30 / 31 / 33",
    "url": "#",
    "color": "orange"
  }, {
    "id": 3,
    "img": "/image/cache/catalog/blackstr/img01120-200x200.jpg",
    "name": "Кофта",
    "attribution": "visualhunt",
    "price": "1.50",
    "stars": "XS / S / M / L / XL",
    "url": "#",
    "color": "green"
  }, {
    "id": 4,
    "img": "/image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg",
    "name": "Джинсы",
    "attribution": "pixabay",
    "price": "1.50",
    "stars": "29 / 30",
    "url": "#",
    "color": "yellow"
  }, {
    "id": 5,
    "img": "/image/cache/catalog/blackstr/xjUme-33bcOoieYgh3C_BOe36Xb5EWi--200x200.jpeg",
    "name": "Спортивные штаны",
    "attribution": "pixabay",
    "price": "4.50",
    "stars": "S / M / L;",
    "url": "#",
    "color": "red"
  }]
}';
		echo $json;die;
	}

	public function carousel(){
        $setting = [
            'name'=>'Home Page',
            'banner_id'=>'8',
            'width'=>'130',
            'height'=>'100',
            'status'=>'1',
        ];
        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

        $data['banners'] = array();

        $results = $this->model_design_banner->getBanner($setting['banner_id']);

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners'][] = array(
                    'title' => $result['title'],
                    'link'  => $result['link'],
                    'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
                );
            }
        }

        $data['module'] = $module++;

        $data['carousel_title'] = $this->language->get('carousel_title');

        echo json_encode(['items'=>[['item'=>$data['banners']]]]);

    }
	public function featured($setting = array()){
        $this->load->language('extension/module/featured');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products'] = array();

        $setting['limit'] = 12;

        $setting['product'] = [43,47,44,31,33,40,28,41,42,30,45,34,36,46,29,48,32,35];

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int)$setting['limit']);
            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                $setting['width'] = 500;
                $setting['height'] = 500;
                if ($product_info) {
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['products'][] = array(
                        'product_id'  => $product_info['product_id'],
                        'thumb'       => $image,
                        'name'        => $product_info['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price'       => $price,
                        'special'     => $special,
                        'tax'         => $tax,
                        'rating'      => $rating,
                        'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                    );
                }
            }
        }

        echo json_encode(['items'=>[['item'=>$data['products']]]]);
    }
	public function brand(){
	    $arr = explode('/',$this->request->get['route']);
	    $id = array_pop($arr);
        $this->load->language('extension/module/featured');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products'] = array();

        $setting['limit'] = 12;


        if($id == 1)
            $setting['product'] = [43,47];
        if($id == 2)
            $setting['product'] = [43,30,45,34,36,46,29,48,32,35];
        if($id == 3)
            $setting['product'] = [43,47,44,31,33,40,28,41,42,30];
        if($id == 4)
            $setting['product'] = [48,32,35];
        if($id == 5)
            $setting['product'] = [43,47,44,31,32,35];
        if($id == 6)
            $setting['product'] = [43,47,44,31,28,41,42,30];
        if($id == 7)
            $setting['product'] = [47,44,31,33,40,28,41,32,35];
        if($id == 8)
        $setting['product'] = [44,31,33,40,28,41,48,32,35];
        if($id == 9)
        $setting['product'] = [43,47,44,48,32,35];
        if($id == 10)
            $setting['product'] = [43,31,33,40,28,41,42,48,32,35];
        if($id == 11)
            $setting['product'] = [43,47,41,42,30,45,34,36,32,35];

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int)$setting['limit']);
            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                $setting['width'] = 500;
                $setting['height'] = 500;
                if ($product_info) {
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['products'][] = array(
                        'product_id'  => $product_info['product_id'],
                        'thumb'       => $image,
                        'name'        => $product_info['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price'       => $price,
                        'special'     => $special,
                        'tax'         => $tax,
                        'rating'      => $rating,
                        'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                    );
                }
            }
        }

        echo json_encode(['items'=>[['item'=>$data['products']]]]);
    }

    public function cart(){
        $this->load->language('common/cart');

        // Totals
        $this->load->model('extension/extension');

        $totals = array();
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total
        );

        // Display prices
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);
        }

        $data['text_empty'] = $this->language->get('text_empty');
        $data['text_cart'] = $this->language->get('text_cart');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_recurring'] = $this->language->get('text_recurring');
        $data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
        $data['text_loading'] = $this->language->get('text_loading');

        $data['button_remove'] = $this->language->get('button_remove');

        $this->load->model('tool/image');
        $this->load->model('tool/upload');

        $data['products'] = array();

        foreach ($this->cart->getProducts() as $product) {
            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], 100, 100);
            } else {
                $image = '';
            }

            $option_data = array();

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
                    'type'  => $option['type']
                );
            }

            // Display prices
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

                $price = $this->currency->format($unit_price, $this->session->data['currency']);
                $total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
            } else {
                $price = false;
                $total = false;
            }

            $data['products'][] = array(
                'cart_id'   => $product['cart_id'],
                'thumb'     => $image,
                'name'      => $product['name'],
                'model'     => $product['model'],
                'option'    => $option_data,
                'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
                'quantity'  => $product['quantity'],
                'price'     => $price,
                'total'     => $total,
                'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }

        // Gift Voucher
        $data['vouchers'] = array();

        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $key => $voucher) {
                $data['vouchers'][] = array(
                    'key'         => $key,
                    'description' => $voucher['description'],
                    'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
                );
            }
        }

        $data['totals'] = array();

        foreach ($totals as $total) {
            $data['totals'][] = array(
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
            );
        }

        $data['cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

        echo json_encode(['items'=>[['item'=>$data['products']]]]);
    }
}
