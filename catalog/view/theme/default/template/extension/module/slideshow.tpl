<div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
  <amp-carousel class="carousel2" width="3" height="1" layout="responsive" type="slides" autoplay>
  <?php foreach ($banners as $banner) { ?>
    <div class="slide">
    <amp-img src="<?php echo $banner['image']; ?>" layout="fill" alt="<?php echo $banner['title']; ?>"></amp-img>
      <a href="<?php echo $banner['link']; ?>" class="caption" target="_blank"><?php echo $banner['title']; ?></a>
    </div>
  <?php } ?>
  </amp-carousel>
</div>