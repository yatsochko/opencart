<div id="search" class="input-group">
  <amp-autocomplete filter="token-prefix" filter-value="city" min-characters="0">
    <input type="search" name="city2" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" autocomplete="off"/>
    <script type="application/json">
      { "items" : [
        {
          "city" : "Спортивные штаны",
          "population" : "XS / S / M / L / XL",
          "image" : "image/cache/catalog/clothes/img01130-200x200.jpg"
        }, {
          "city" : "Джинсы",
          "population" : "30 / 31 / 36",
          "image" : "image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg"
        }, {
          "city" : "Спортивные штаны",
          "population" : "29 / 30 / 31 / 33",
          "image" : "image/cache/catalog/clothes/img01133-200x200.jpg"
        },{
          "city" : "Спортивные штаны",
          "population" : "XS / S / M / L / XL",
          "image" : "image/cache/catalog/clothes/img01130-200x200.jpg"
        }, {
          "city" : "Джинсы",
          "population" : "30 / 31 / 36",
          "image" : "image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg"
        },{
          "city" : "Спортивные штаны",
          "population" : "XS / S / M / L / XL",
          "image" : "image/cache/catalog/clothes/img01130-200x200.jpg"
        }, {
          "city" : "Джинсы",
          "population" : "30 / 31 / 36",
          "image" : "image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg"
        },{
          "city" : "Спортивные штаны",
          "population" : "XS / S / M / L / XL",
          "image" : "image/cache/catalog/clothes/img01130-200x200.jpg"
        }, {
          "city" : "Джинсы",
          "population" : "30 / 31 / 36",
          "image" : "image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg"
        },{
          "city" : "Спортивные штаны",
          "population" : "XS / S / M / L / XL",
          "image" : "image/cache/catalog/clothes/img01130-200x200.jpg"
        }, {
          "city" : "Джинсы",
          "population" : "30 / 31 / 36",
          "image" : "image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg"
        },{
          "city" : "Спортивные штаны",
          "population" : "XS / S / M / L / XL",
          "image" : "image/cache/catalog/clothes/img01130-200x200.jpg"
        }, {
          "city" : "Джинсы",
          "population" : "30 / 31 / 36",
          "image" : "image/cache/catalog/clothes/biuQ9ZG2ECiaqO-UQ7U5a_DxMB6KRDNH-200x200.jpg"
        }
      ] }
    </script>
    <template type="amp-mustache" id="amp-template-custom">
      <div class="search-item" data-value="{{city}}">
        <amp-img class="trending" width="36" height="36" src="{{image}}"></amp-img>
        <div style="color:#000;">
          <div>{{city}}</div>
          <div class="custom-population">{{population}}</div>
        </div>
      </div>
    </template>
  </amp-autocomplete>

  <span class="input-group-btn">
    <button type="button" class="btn btn-default btn-lg"><svg style="width:20px;height:20px;color:black" viewBox="0 0 24 24"> <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" /> </svg></button>
  </span>
</div>