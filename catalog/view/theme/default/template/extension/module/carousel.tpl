<div id="carousel<?php echo $module; ?>" class="owl-carousel">
  <amp-list id="carouselList" reset-on-refresh layout="fixed-height" height="110" src="index.php?route=api/json/carousel" binding="no">
    <div placeholder style="display:flex">
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
      <div style="display:block;min-width:100px;height:100px;border:1px solid #ccc;margin:4px"></div>
    </div>
    <template type="amp-mustache">
      <amp-carousel height="110" width="auto" layout="fixed-height" type="carousel" aria-label="Basic usage carousel">
        {{#item}}
        <amp-img role="button" tabindex="0" on="tap:AMP.setState({featured_list:'{{link}}'})" src="{{image}}" width="110" height="110" alt="{{title}}"></amp-img>
        {{/item}}
      </amp-carousel>
    </template>
  </amp-list>
</div>
