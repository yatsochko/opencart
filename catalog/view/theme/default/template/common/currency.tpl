<?php if (count($currencies) > 1) { ?>

<?php } ?>
<amp-mega-menu height="30" layout="fixed-height">
  <nav>
    <ul>
      <li class="menu-item">
        <span role="button">
          <?php foreach ($currencies as $currency) { ?>
          <strong><?php echo $currency['symbol_right']; ?></strong>
          <?php } ?>
        </span>
        <div role="dialog">
          <ol>
            <?php foreach ($currencies as $currency) { ?>
            <?php if ($currency['symbol_left']) { ?>
            <li><button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?> <?php echo $currency['title']; ?></button></li>
            <?php } else { ?>
            <li><button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?> <?php echo $currency['title']; ?></button></li>
            <?php } ?>
            <?php } ?>
          </ol>
        </div>

      </li>
      <li class="menu-item">
        <span role="button">
           <?php foreach ($languages as $language) { ?>
          <?php if ($language['code'] == $code) { ?>
          <amp-img layout="fixed" height="11" width="16" src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>"></amp-img>
          <?php } ?>
          <?php } ?>
          <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_language; ?></span>
        </span>
        <div role="dialog">
          <ol>
            <?php foreach ($languages as $language) { ?>
            <li><button class="btn btn-link btn-block language-select" type="button" name="<?php echo $language['code']; ?>"><amp-img  layout="fixed" height="11" width="16" src="catalog/language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /></amp-img> <?php echo $language['name']; ?></button></li>
            <?php } ?>
          </ol>
        </div>
      </li>
    </ul>
  </nav>
</amp-mega-menu>